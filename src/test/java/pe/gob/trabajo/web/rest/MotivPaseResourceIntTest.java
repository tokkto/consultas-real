package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.ConsultasApp;

import pe.gob.trabajo.domain.MotivPase;
import pe.gob.trabajo.repository.MotivPaseRepository;
import pe.gob.trabajo.repository.search.MotivPaseSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MotivPaseResource REST controller.
 *
 * @see MotivPaseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConsultasApp.class)
public class MotivPaseResourceIntTest {

    private static final String DEFAULT_V_OBSMOTPAS = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSMOTPAS = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private MotivPaseRepository motivPaseRepository;

    @Autowired
    private MotivPaseSearchRepository motivPaseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMotivPaseMockMvc;

    private MotivPase motivPase;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotivPaseResource motivPaseResource = new MotivPaseResource(motivPaseRepository, motivPaseSearchRepository);
        this.restMotivPaseMockMvc = MockMvcBuilders.standaloneSetup(motivPaseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotivPase createEntity(EntityManager em) {
        MotivPase motivPase = new MotivPase()
            .vObsmotpas(DEFAULT_V_OBSMOTPAS)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return motivPase;
    }

    @Before
    public void initTest() {
        motivPaseSearchRepository.deleteAll();
        motivPase = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotivPase() throws Exception {
        int databaseSizeBeforeCreate = motivPaseRepository.findAll().size();

        // Create the MotivPase
        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isCreated());

        // Validate the MotivPase in the database
        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeCreate + 1);
        MotivPase testMotivPase = motivPaseList.get(motivPaseList.size() - 1);
        assertThat(testMotivPase.getvObsmotpas()).isEqualTo(DEFAULT_V_OBSMOTPAS);
        assertThat(testMotivPase.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testMotivPase.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testMotivPase.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testMotivPase.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testMotivPase.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testMotivPase.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testMotivPase.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the MotivPase in Elasticsearch
        MotivPase motivPaseEs = motivPaseSearchRepository.findOne(testMotivPase.getId());
        assertThat(motivPaseEs).isEqualToComparingFieldByField(testMotivPase);
    }

    @Test
    @Transactional
    public void createMotivPaseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motivPaseRepository.findAll().size();

        // Create the MotivPase with an existing ID
        motivPase.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isBadRequest());

        // Validate the MotivPase in the database
        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motivPaseRepository.findAll().size();
        // set the field null
        motivPase.setnUsuareg(null);

        // Create the MotivPase, which fails.

        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isBadRequest());

        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = motivPaseRepository.findAll().size();
        // set the field null
        motivPase.settFecreg(null);

        // Create the MotivPase, which fails.

        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isBadRequest());

        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = motivPaseRepository.findAll().size();
        // set the field null
        motivPase.setnFlgactivo(null);

        // Create the MotivPase, which fails.

        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isBadRequest());

        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = motivPaseRepository.findAll().size();
        // set the field null
        motivPase.setnSedereg(null);

        // Create the MotivPase, which fails.

        restMotivPaseMockMvc.perform(post("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isBadRequest());

        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMotivPases() throws Exception {
        // Initialize the database
        motivPaseRepository.saveAndFlush(motivPase);

        // Get all the motivPaseList
        restMotivPaseMockMvc.perform(get("/api/motiv-pases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivPase.getId().intValue())))
            .andExpect(jsonPath("$.[*].vObsmotpas").value(hasItem(DEFAULT_V_OBSMOTPAS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getMotivPase() throws Exception {
        // Initialize the database
        motivPaseRepository.saveAndFlush(motivPase);

        // Get the motivPase
        restMotivPaseMockMvc.perform(get("/api/motiv-pases/{id}", motivPase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motivPase.getId().intValue()))
            .andExpect(jsonPath("$.vObsmotpas").value(DEFAULT_V_OBSMOTPAS.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingMotivPase() throws Exception {
        // Get the motivPase
        restMotivPaseMockMvc.perform(get("/api/motiv-pases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotivPase() throws Exception {
        // Initialize the database
        motivPaseRepository.saveAndFlush(motivPase);
        motivPaseSearchRepository.save(motivPase);
        int databaseSizeBeforeUpdate = motivPaseRepository.findAll().size();

        // Update the motivPase
        MotivPase updatedMotivPase = motivPaseRepository.findOne(motivPase.getId());
        updatedMotivPase
            .vObsmotpas(UPDATED_V_OBSMOTPAS)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restMotivPaseMockMvc.perform(put("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMotivPase)))
            .andExpect(status().isOk());

        // Validate the MotivPase in the database
        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeUpdate);
        MotivPase testMotivPase = motivPaseList.get(motivPaseList.size() - 1);
        assertThat(testMotivPase.getvObsmotpas()).isEqualTo(UPDATED_V_OBSMOTPAS);
        assertThat(testMotivPase.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testMotivPase.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testMotivPase.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testMotivPase.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testMotivPase.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testMotivPase.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testMotivPase.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the MotivPase in Elasticsearch
        MotivPase motivPaseEs = motivPaseSearchRepository.findOne(testMotivPase.getId());
        assertThat(motivPaseEs).isEqualToComparingFieldByField(testMotivPase);
    }

    @Test
    @Transactional
    public void updateNonExistingMotivPase() throws Exception {
        int databaseSizeBeforeUpdate = motivPaseRepository.findAll().size();

        // Create the MotivPase

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMotivPaseMockMvc.perform(put("/api/motiv-pases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motivPase)))
            .andExpect(status().isCreated());

        // Validate the MotivPase in the database
        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMotivPase() throws Exception {
        // Initialize the database
        motivPaseRepository.saveAndFlush(motivPase);
        motivPaseSearchRepository.save(motivPase);
        int databaseSizeBeforeDelete = motivPaseRepository.findAll().size();

        // Get the motivPase
        restMotivPaseMockMvc.perform(delete("/api/motiv-pases/{id}", motivPase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean motivPaseExistsInEs = motivPaseSearchRepository.exists(motivPase.getId());
        assertThat(motivPaseExistsInEs).isFalse();

        // Validate the database is empty
        List<MotivPase> motivPaseList = motivPaseRepository.findAll();
        assertThat(motivPaseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMotivPase() throws Exception {
        // Initialize the database
        motivPaseRepository.saveAndFlush(motivPase);
        motivPaseSearchRepository.save(motivPase);

        // Search the motivPase
        restMotivPaseMockMvc.perform(get("/api/_search/motiv-pases?query=id:" + motivPase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motivPase.getId().intValue())))
            .andExpect(jsonPath("$.[*].vObsmotpas").value(hasItem(DEFAULT_V_OBSMOTPAS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotivPase.class);
        MotivPase motivPase1 = new MotivPase();
        motivPase1.setId(1L);
        MotivPase motivPase2 = new MotivPase();
        motivPase2.setId(motivPase1.getId());
        assertThat(motivPase1).isEqualTo(motivPase2);
        motivPase2.setId(2L);
        assertThat(motivPase1).isNotEqualTo(motivPase2);
        motivPase1.setId(null);
        assertThat(motivPase1).isNotEqualTo(motivPase2);
    }
}
