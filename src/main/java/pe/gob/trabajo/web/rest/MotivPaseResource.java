package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.MotivPase;

import pe.gob.trabajo.repository.MotivPaseRepository;
import pe.gob.trabajo.repository.search.MotivPaseSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MotivPase.
 */
@RestController
@RequestMapping("/api")
public class MotivPaseResource {

    private final Logger log = LoggerFactory.getLogger(MotivPaseResource.class);

    private static final String ENTITY_NAME = "motivPase";

    private final MotivPaseRepository motivPaseRepository;

    private final MotivPaseSearchRepository motivPaseSearchRepository;

    public MotivPaseResource(MotivPaseRepository motivPaseRepository, MotivPaseSearchRepository motivPaseSearchRepository) {
        this.motivPaseRepository = motivPaseRepository;
        this.motivPaseSearchRepository = motivPaseSearchRepository;
    }

    /**
     * POST  /motiv-pases : Create a new motivPase.
     *
     * @param motivPase the motivPase to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motivPase, or with status 400 (Bad Request) if the motivPase has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motiv-pases")
    @Timed
    public ResponseEntity<MotivPase> createMotivPase(@Valid @RequestBody MotivPase motivPase) throws URISyntaxException {
        log.debug("REST request to save MotivPase : {}", motivPase);
        if (motivPase.getId() != null) {
            throw new BadRequestAlertException("A new motivPase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        motivPase.tFecreg(Instant.now());
        MotivPase result = motivPaseRepository.save(motivPase);
        motivPaseSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/motiv-pases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /motiv-pases : Updates an existing motivPase.
     *
     * @param motivPase the motivPase to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motivPase,
     * or with status 400 (Bad Request) if the motivPase is not valid,
     * or with status 500 (Internal Server Error) if the motivPase couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/motiv-pases")
    @Timed
    public ResponseEntity<MotivPase> updateMotivPase(@Valid @RequestBody MotivPase motivPase) throws URISyntaxException {
        log.debug("REST request to update MotivPase : {}", motivPase);
        if (motivPase.getId() == null) {
            return createMotivPase(motivPase);
        }
        motivPase.tFecupd(Instant.now());
        MotivPase result = motivPaseRepository.save(motivPase);
        motivPaseSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motivPase.getId().toString()))
            .body(result);
    }

    /**
     * GET  /motiv-pases : get all the motivPases.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of motivPases in body
     */
    @GetMapping("/motiv-pases")
    @Timed
    public List<MotivPase> getAllMotivPases() {
        log.debug("REST request to get all MotivPases");
        return motivPaseRepository.findAll();
        }

    /**
     * GET  /motiv-pases/:id : get the "id" motivPase.
     *
     * @param id the id of the motivPase to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motivPase, or with status 404 (Not Found)
     */
    @GetMapping("/motiv-pases/{id}")
    @Timed
    public ResponseEntity<MotivPase> getMotivPase(@PathVariable Long id) {
        log.debug("REST request to get MotivPase : {}", id);
        MotivPase motivPase = motivPaseRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motivPase));
    }

    /** JH
     * GET  /motiv-pases : get all the motiv-pases.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of motiv-pases in body
     */
    @GetMapping("/motiv-pases/activos")
    @Timed
    public List<MotivPase> getAll_Activos() {
        log.debug("REST request to get all motiv-pases");
        return motivPaseRepository.findAll_Activos();
    }

     /** JH
     * GET  /motiv-pases/pasegl/id/:id_pase :
     * @param id_pase es el id del Pasegl
     * @return the ResponseEntity with status 200 (OK) and with body the MotivPase, or with status 404 (Not Found)
     */
	@GetMapping("/motiv-pases/pasegl/id/{id_pase}")
    @Timed
    public List<MotivPase> getListMotivPaseById_Pasegl(@PathVariable Long id_pase) {
        log.debug("REST request to get MotivPase : id_pase {}", id_pase);
        return motivPaseRepository.findListMotivPaseById_Pasegl(id_pase);
    }

     /** JH
     * GET  /motiv-pases/idoficina/:id_ofi/idpase/:id_pase :
     * @param id_ofi es el id de la Oficina
     * @return the ResponseEntity with status 200 (OK) and with body the MotivPase, or with status 404 (Not Found)
     */
	@GetMapping("/motiv-pases/idoficina/{id_ofi}/idpase/{id_pase}")
    @Timed
    public List<MotivPase> getListMotivPaseByIdOfic_IdPase(@PathVariable Long id_ofi, @PathVariable Long id_pase) {
        log.debug("REST request to get MotivPase : id_ofi {} id_pase {}", id_ofi,id_pase);
        return motivPaseRepository.findListMotivPaseByIdOfic_IdPase(id_ofi,id_pase);
    }

    /**
     * DELETE  /motiv-pases/:id : delete the "id" motivPase.
     *
     * @param id the id of the motivPase to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/motiv-pases/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotivPase(@PathVariable Long id) {
        log.debug("REST request to delete MotivPase : {}", id);
        motivPaseRepository.delete(id);
        motivPaseSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/motiv-pases?query=:query : search for the motivPase corresponding
     * to the query.
     *
     * @param query the query of the motivPase search
     * @return the result of the search
     */
    @GetMapping("/_search/motiv-pases")
    @Timed
    public List<MotivPase> searchMotivPases(@RequestParam String query) {
        log.debug("REST request to search MotivPases for query {}", query);
        return StreamSupport
            .stream(motivPaseSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
