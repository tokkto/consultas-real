package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Accionadop.
 */
@Entity
@Table(name = "gltbc_accionadop")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "gltbc_accionadop")
public class Accionadop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codacadop", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_desaccdop", length = 100, nullable = false)
    private String vDesaccdop;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_bndaccion", length = 1, nullable = false)
    private String vBndaccion;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "accionadop")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Accadoate> accadoates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDesaccdop() {
        return vDesaccdop;
    }

    public Accionadop vDesaccdop(String vDesaccdop) {
        this.vDesaccdop = vDesaccdop;
        return this;
    }

    public void setvDesaccdop(String vDesaccdop) {
        this.vDesaccdop = vDesaccdop;
    }

    public String getvBndaccion() {
        return vBndaccion;
    }

    public Accionadop vBndaccion(String vBndaccion) {
        this.vBndaccion = vBndaccion;
        return this;
    }

    public void setvBndaccion(String vBndaccion) {
        this.vBndaccion = vBndaccion;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Accionadop nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Accionadop tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Accionadop nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Accionadop nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Accionadop nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Accionadop tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Accionadop nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Accadoate> getAccadoates() {
        return accadoates;
    }

    public Accionadop accadoates(Set<Accadoate> accadoates) {
        this.accadoates = accadoates;
        return this;
    }

    public Accionadop addAccadoate(Accadoate accadoate) {
        this.accadoates.add(accadoate);
        accadoate.setAccionadop(this);
        return this;
    }

    public Accionadop removeAccadoate(Accadoate accadoate) {
        this.accadoates.remove(accadoate);
        accadoate.setAccionadop(null);
        return this;
    }

    public void setAccadoates(Set<Accadoate> accadoates) {
        this.accadoates = accadoates;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Accionadop accionadop = (Accionadop) o;
        if (accionadop.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accionadop.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Accionadop{" +
            "id=" + getId() +
            ", vDesaccdop='" + getvDesaccdop() + "'" +
            ", vBndaccion='" + getvBndaccion() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
