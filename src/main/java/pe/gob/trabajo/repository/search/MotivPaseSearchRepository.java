package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.MotivPase;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MotivPase entity.
 */
public interface MotivPaseSearchRepository extends ElasticsearchRepository<MotivPase, Long> {
}
