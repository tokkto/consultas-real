package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.MotivPase;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the MotivPase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotivPaseRepository extends JpaRepository<MotivPase, Long> {

    @Query("select motivPase from MotivPase motivPase where motivPase.nFlgactivo = true")
    List<MotivPase> findAll_Activos();

    // @Query("select motivPase " + 
    @Query("select motivPase.id, motivPase.vObsmotpas, motivPase.motatenofic.motate.vDesmotate " + 
            "from MotivPase motivPase where motivPase.pasegl.id=?1 and motivPase.nFlgactivo = true")
    List<MotivPase> findListMotivPaseById_Pasegl(Long id);

    // @Query("select new map(motatenofic.id as nCodmtatof, motivPase.pasegl.id as nCodpase, motatenofic.motate.vDesmotate as vDesmotate, motivPase.id as nCodmotpas, motivPase.vObsmotpas as vObsmotpas, motivPase.nFlgactivo as motivpase_nFlgactivo)" +
    @Query("select new map(motatenofic as Motateno, motivPase.id as idmotpase, motivPase.vObsmotpas as observacion) " +
    " from Motatenofic motatenofic left join MotivPase motivPase on (motatenofic.id=motivPase.motatenofic.id and motivPase.pasegl.id=?2 and motivPase.nFlgactivo=true) " +
    " where motatenofic.oficina.id=?1 and motatenofic.nFlgactivo = true")
    List<MotivPase> findListMotivPaseByIdOfic_IdPase(Long id_ofi, Long id_pase);
}
